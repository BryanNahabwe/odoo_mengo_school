# Copyright 2009-2018 Noviat
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    def export_xls(self):
        # print(__name__)
        # print(__name__.split('addons.'))
        # print(__name__.split('addons.')[1])

        module = __name__.split('addons.')[1].split('.')[0]
        # print(module)
        report_name = '{}.partner_export_xlsx'.format(module)
        # print('{}.partner_export_xlsx')
        # print(report_name)
        # print(dict)
        # print(self.env.context)
        report = {
            'type': 'ir.actions.report',
            'report_type': 'xlsx',
            'report_name': report_name,
            # model name will be used if no report_file passed via context
            'context': dict(self.env.context, report_file='partner'),
            # report_xlsx doesn't pass the context if the data dict is empty
            # cf. report_xlsx\static\src\js\report\qwebactionmanager.js
            # TODO: create PR on report_xlsx to fix this
            'data': {'dynamic_report': True},
        }
        # print(report)
        return report
