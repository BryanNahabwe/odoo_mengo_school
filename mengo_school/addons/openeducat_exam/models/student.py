from odoo import api, fields, models


class OpStudentResult(models.Model):
    _inherit = "op.student"

    result_line_ids = fields.One2many('op.exam.attendees', 'student_id', 'Result Details')
