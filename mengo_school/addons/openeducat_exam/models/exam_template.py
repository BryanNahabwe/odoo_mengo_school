from odoo import api, fields, models


class OpExamTemplate(models.Model):
    _name = 'op.exam.template'
    _rec_name = "student_id"
    _description = 'New Exam Template'

    student_id = fields.Many2one('op.student', 'Student', readonly=True)
    registration_number = fields.Char('Registration Number', readonly=True)
    course_id = fields.Many2one('op.course', 'Course', readonly=True)
    semester = fields.Selection([('Semester 1', 'Semester 1'),
                                 ('Semester 2', 'Semester 2'),
                                 ('Trimester 1', 'Trimester 1'),
                                 ('Trimester 2', 'Trimester 2'),
                                 ('Trimester 3', 'Trimester 3'),
                                 ('Module 1', 'Module 1'),
                                 ('Module 2', 'Module 2'),
                                 ('Module 3', 'Module 3'),
                                 ('Module 4', 'Module 4'),
                                 ('Module 5', 'Module 5'),
                                 ('Module 6', 'Module 6'),
                                 ('Module 7', 'Module 7')], readonly=True,
                                string='Study Period')
    year_of_study = fields.Selection([('Year 1', 'Year 1'), ('Year 2', 'Year 2'), ('Year 3', 'Year 3'),
                                      ('Year 4', 'Year 4'), ('Year 5', 'Year 5')], string='Year of Study',
                                     readonly=True)
    total_credit_units = fields.Float("Total Credit Units", compute='_compute_credit_units', store=True, readonly=True)
    gpa = fields.Float("GPA", compute='_compute_gpa', store=True, readonly=True)
    cgpa = fields.Float("CGPA", compute='_compute_cgpa', store=True, readonly=True)
    result_line = fields.Many2many('op.exam.attendees', string='Results', readonly=True)

    @api.multi
    @api.depends('result_line.grade_weightage')
    def _compute_credit_units(self):
        for record in self:
            total_credit_units = sum([float(x.grade_weightage) for x in record.result_line])
            record.total_credit_units = total_credit_units

    @api.multi
    @api.depends('total_credit_units')
    def _compute_gpa(self):
        for record in self:
            total_grade_points = sum([float(x.grade_point) * float(x.grade_weightage) for x in record.result_line])
            record.gpa = total_grade_points / record.total_credit_units

    @api.multi
    @api.depends('gpa')
    def _compute_cgpa(self):
        for record in self:
            rec = self.env['op.exam.template'].search([('student_id', '=', record.student_id.id)])
            count = self.env['op.exam.template'].search_count([('student_id', '=', record.student_id.id)])
            if rec:
                sum_gpa = 0
                for line in rec:
                    sum_gpa = sum_gpa + line.gpa
                record.cgpa = sum_gpa / count
            else:
                record.cgpa = record.gpa
