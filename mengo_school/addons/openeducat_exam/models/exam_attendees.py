# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from datetime import datetime
from dateutil.relativedelta import relativedelta
import io
import uuid

from odoo import models, fields, api, _, tools
from odoo.exceptions import ValidationError
from odoo.tools import ormcache


class OpExamAttendees(models.Model):
    _name = "op.exam.attendees"
    _rec_name = "student_id"
    _description = "Exam Attendees"

    student_id = fields.Many2one('op.student', 'Student Name', required=True)
    registration_number = fields.Char('Registration Number')
    progressive_marks = fields.Float('Progressive Marks')
    exam_marks = fields.Float('Summative Marks')
    final_marks = fields.Float('Final Marks', readonly=True, compute='_compute_final_marks', store=True)
    grade = fields.Char('Grade', readonly=True, compute='_compute_grade', store=True)
    grade_point = fields.Float('Grade Point', readonly=True, compute='_compute_grade_point', store=True)
    exam_id = fields.Many2one('op.exam', 'Exam', required=True, ondelete="cascade")
    exam_session = fields.Many2one('op.exam.session', 'Exam Session')
    exam_name = fields.Char('Exam Name', required=True)
    course_id = fields.Many2one(
        'op.course', related='exam_id.course_id', store=True,
        readonly=True)
    subject_id = fields.Many2one(
        'op.subject', related='exam_id.subject_id', store=True,
        readonly=True)
    subject_code = fields.Char(related='exam_id.subject_code', store=True, readonly=True)
    grade_weightage = fields.Float(related='exam_id.grade_weightage', store=True, readonly=True)
    grade_ids = fields.Many2many('op.grade.configuration', related='exam_id.course_id.grade_ids',
                                 string='Grade Configuration')
    semester = fields.Selection([('Semester 1', 'Semester 1'),
                                 ('Semester 2', 'Semester 2'),
                                 ('Trimester 1', 'Trimester 1'),
                                 ('Trimester 2', 'Trimester 2'),
                                 ('Trimester 3', 'Trimester 3')],
                                string='Study Period', related='exam_id.semester', store=True,
                                readonly=True)
    year_of_study = fields.Selection([('Year 1', 'Year 1'), ('Year 2', 'Year 2'), ('Year 3', 'Year 3'),
                                      ('Year 4', 'Year 4'), ('Year 5', 'Year 5')], 'Year of Study',
                                     related='exam_id.year_of_study', store=True,
                                     readonly=True)
    record_external_id = fields.Char('External ID')
    year = fields.Char('Year', default=lambda self: self._get_year(), copy=False, readonly=True, store=True)
    status = fields.Selection([('pass', 'Pass'), ('fail', 'Fail'), ('not_started', 'Not Started'), ('absent', 'Absent')],
                              'Status',
                              compute='_compute_status', readonly=True, store=True)

    # _sql_constraints = [
    #     ('unique_attendees',
    #      'unique(student_id,exam_id,registration_number,year)',
    #      'Students must be unique Per Exam, Per Registration number and Per Year.'),
    # ]

    @api.constrains('progressive_marks')
    def _check_progressive_marks(self):
        for res in self:
            if (res.progressive_marks < 0.0) or (res.progressive_marks > 40.0):
                raise ValidationError(_('Progressive marks ranges between 0 and 40!'))

    @api.constrains('exam_marks')
    def _check_exam_marks(self):
        for res in self:
            if (res.exam_marks < 0.0) or (res.exam_marks > 60.0):
                raise ValidationError(_('Summative marks ranges between 0 and 60!'))

    @api.model
    def _get_year(self):
        today_year = str(fields.Date.today().year)
        return today_year

    @api.onchange('exam_id')
    def onchange_exam(self):
        if self.exam_id:
            self.course_id = self.exam_id.session_id.course_id
            self.exam_name = self.exam_id.subject_id.name
            self.student_id = False

    @api.multi
    @api.depends('progressive_marks', 'exam_marks')
    def _compute_final_marks(self):
        for record in self:
            record.final_marks = record.progressive_marks + record.exam_marks

    @api.multi
    @api.depends('final_marks')
    def _compute_grade(self):
        for record in self:
            if record.exam_id.session_id.evaluation_type == 'grade':
                grades = record.grade_ids
                for grade in grades:
                    if grade.min_per <= record.final_marks <= grade.max_per:
                        record.grade = grade.result

    @api.multi
    @api.depends('final_marks')
    def _compute_grade_point(self):
        for record in self:
            if record.exam_id.session_id.evaluation_type == 'grade':
                grades = record.grade_ids
                for grade in grades:
                    if grade.min_per <= record.final_marks <= grade.max_per:
                        record.grade_point = grade.grade_point

    @api.multi
    @api.depends('final_marks')
    def _compute_status(self):
        for record in self:
            if record.final_marks < record.exam_id.min_marks:
                record.status = 'fail'
            elif record.final_marks > record.exam_id.min_marks:
                record.status = 'pass'
            else:
                record.status = 'absent'

    @api.model
    @ormcache()
    def _is_an_ordinary_table(self):
        return tools.table_kind(self.env.cr, self._table) == 'r'

    @api.multi
    @api.constrains('student_id')
    def insert_xml_id(self, skip=False):
        """ Create missing external ids for records in ``self``, and return an
            iterator of pairs ``(record, xmlid)`` for the records in ``self``.

        :rtype: Iterable[Model, str | None]
        """
        if skip:
            return ((record, None) for record in self)

        if not self:
            return iter([])

        if not self._is_an_ordinary_table():
            raise Exception(
                "You can not export the column ID of model %s, because the "
                "table %s is not an ordinary table."
                % (self._name, self._table))

        modname = 'student_'

        cr = self.env.cr
        cr.execute("""
                      SELECT res_id, module, name
                      FROM ir_model_data
                      WHERE model = %s AND res_id in %s
                  """, (self._name, tuple(self.ids)))

        xids = {
            res_id: (module, name)
            for res_id, module, name in cr.fetchall()
        }

        def to_xid(record_id):
            (module, name) = xids[record_id]
            return ('%s.%s' % (module, name)) if module else name

        # create missing xml ids
        missing = self.filtered(lambda r: r.id not in xids)
        if not missing:
            return (
                (record, to_xid(record.id))
                for record in self
            )

        xids.update(
            (r.id, (modname, '%s_%s' % (
                r.id,
                uuid.uuid4().hex[:8],
            )))
            for r in missing
        )

        fields = ['module', 'model', 'name', 'res_id', 'date_init']

        now = datetime.now()
        for record in missing:
            self.record_external_id = ".".join([modname, xids[record.id][1]])
            cr.copy_from(io.StringIO("%s\t%s\t%s\t%d\t%s" % (
                modname,
                record._name,
                xids[record.id][1],
                record.id,
                now.strftime("%Y/%m/%d %H:%M:%S"),
            )), table='ir_model_data', columns=fields, )

        self.env['ir.model.data'].invalidate_cache(fnames=fields)

        return (
            (record, to_xid(record.id))
            for record in self
        )
