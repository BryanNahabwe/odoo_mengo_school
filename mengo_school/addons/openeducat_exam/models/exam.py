# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import datetime

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class OpExam(models.Model):
    _name = "op.exam"
    _inherit = "mail.thread"
    _rec_name = "subject_id"
    _description = "Exam"

    session_id = fields.Many2one('op.exam.session', 'Exam Session',
                                 domain=[('state', 'not in',
                                          ['cancel', 'done'])], states={'done': [('readonly', True)]})
    course_id = fields.Many2one('op.course', string="Course", required=True, states={'done': [('readonly', True)]})
    semester = fields.Selection([('Semester 1', 'Semester 1'),
                                 ('Semester 2', 'Semester 2'),
                                 ('Trimester 1', 'Trimester 1'),
                                 ('Trimester 2', 'Trimester 2'),
                                 ('Trimester 3', 'Trimester 3'),
                                 ('Module 1', 'Module 1'),
                                 ('Module 2', 'Module 2'),
                                 ('Module 3', 'Module 3'),
                                 ('Module 4', 'Module 4'),
                                 ('Module 5', 'Module 5'),
                                 ('Module 6', 'Module 6'),
                                 ('Module 7', 'Module 7')], default='Semester 1',
                                string='Study Period', required=True,
                                states={'done': [('readonly', True)]})
    subject_ids = fields.Many2many('op.subject', string='Course Units')
    subject_id = fields.Many2one('op.subject', string='Course Unit', required=True,
                                 states={'done': [('readonly', True)]})
    subject_code = fields.Char('Course unit Code', size=16, required=True, states={'done': [('readonly', True)]})
    grade_weightage = fields.Float('Credit Units', states={'done': [('readonly', True)]})
    attendees_line = fields.One2many(
        'op.exam.attendees', 'exam_id', 'Attendees', readonly=True)
    start_time = fields.Datetime('Start Time', required=True, states={'done': [('readonly', True)]})
    end_time = fields.Datetime('End Time', required=True, states={'done': [('readonly', True)]})
    state = fields.Selection(
        [('draft', 'Draft'), ('schedule', 'Scheduled'), ('held', 'Held'),
         ('result_updated', 'Result Updated'),
         ('cancel', 'Cancelled'), ('done', 'Done')], 'State',
        readonly=True, default='draft', track_visibility='onchange')
    year_of_study = fields.Selection([('Year 1', 'Year 1'), ('Year 2', 'Year 2'), ('Year 3', 'Year 3'),
                                      ('Year 4', 'Year 4'), ('Year 5', 'Year 5')], 'Year of Study',
                                     required=True,
                                     default='Year 1', track_visibility='onchange',
                                     states={'done': [('readonly', True)]})
    # note = fields.Text('Note')
    responsible_id = fields.Many2many('op.faculty', string='Responsible', states={'done': [('readonly', True)]})
    total_marks = fields.Integer('Total Marks', required=True, default=100, states={'done': [('readonly', True)]})
    min_marks = fields.Integer('Passing Marks', required=True, default=50, states={'done': [('readonly', True)]})

    _sql_constraints = [
        ('unique_exam',
         'unique(session_id,course_id,subject_id,year_of_study)',
         'Exam must be unique Per Session, per Course, Per Course Unit and Per Year of Study.'),
    ]

    @api.constrains('total_marks', 'min_marks')
    def _check_marks(self):
        if self.total_marks <= 0.0 or self.min_marks <= 0.0:
            raise ValidationError(_('Enter proper marks!'))
        if self.min_marks > self.total_marks:
            raise ValidationError(_(
                "Passing Marks can't be greater than Total Marks"))

    @api.onchange('session_id')
    def onchange_session(self):
        if self.session_id:
            self.semester = self.session_id.semester

    @api.onchange('course_id')
    def onchange_course(self):
        if self.course_id:
            cd = self.course_id
            self.subject_id = False
            self.subject_ids = [(6, 0, [subj.id for subj in cd.subject_ids])]

    @api.onchange('subject_id')
    def onchange_subject(self):
        if self.subject_id:
            subj = self.subject_id
            self.subject_code = subj.code or False
            self.grade_weightage = subj.grade_weightage or False

    @api.constrains('start_time', 'end_time')
    def _check_date_time(self):
        session_start = datetime.datetime.combine(
            fields.Date.from_string(self.session_id.start_date),
            datetime.time.min)
        session_end = datetime.datetime.combine(
            fields.Date.from_string(self.session_id.end_date),
            datetime.time.max)
        start_time = fields.Datetime.from_string(self.start_time)
        end_time = fields.Datetime.from_string(self.end_time)
        if start_time > end_time:
            raise ValidationError(_('End Time cannot be set \
            before Start Time.'))
        elif start_time < session_start or start_time > session_end or \
                end_time < session_start or end_time > session_end:
            raise ValidationError(
                _('Exam Time Table should in between Exam Session Dates.'))

    @api.multi
    def act_result_updated(self):
        self.state = 'result_updated'

    @api.multi
    def act_done(self):
        self.state = 'done'

    @api.multi
    def act_draft(self):
        self.state = 'draft'

    @api.multi
    def act_cancel(self):
        self.state = 'cancel'

    @api.multi
    def export_student_xls(self):
        module = __name__.split('addons.')[1].split('.')[0]
        report_name = '{}.students_export_xlsx'.format(module)
        report = {
            'type': 'ir.actions.report',
            'report_type': 'xlsx',
            'report_name': report_name,
            # model name will be used if no report_file passed via context
            'context': dict(self.env.context, report_file='Students'),
            # report_xlsx doesn't pass the context if the data dict is empty
            # cf. report_xlsx\static\src\js\report\qwebactionmanager.js
            # TODO: create PR on report_xlsx to fix this
            'data': {'dynamic_report': True},
        }
        return report
