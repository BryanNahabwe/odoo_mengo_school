# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class OpResultLine(models.Model):
    _name = "op.result.line"
    _rec_name = "student_id"
    _description = "Result Line"

    marksheet_line_id = fields.Many2one(
        'op.marksheet.line', 'Marksheet Line')
    exam_id = fields.Many2one('op.exam', 'Exam', required=True)
    evaluation_type = fields.Selection(
        related='exam_id.session_id.evaluation_type', store=True)
    registration_number = fields.Char('Registration Number')
    progressive_marks = fields.Integer('Progressive Marks')
    exam_marks = fields.Integer('Summative Marks')
    final_marks = fields.Integer('Final Marks')
    grade = fields.Char('Grade')
    grade_point = fields.Float('Grade Point')
    student_id = fields.Many2one('op.student', 'Student', required=True)
    status = fields.Selection([('pass', 'Pass'), ('fail', 'Fail')], 'Status',
                              compute='_compute_status', store=True)

    @api.multi
    @api.depends('final_marks')
    def _compute_status(self):
        for record in self:
            record.status = 'pass'
            if record.final_marks < record.exam_id.min_marks:
                record.status = 'fail'
