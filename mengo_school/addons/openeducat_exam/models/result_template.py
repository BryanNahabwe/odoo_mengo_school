# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from collections import defaultdict

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class OpResultTemplate(models.Model):
    _name = "op.result.template"
    _inherit = ["mail.thread"]
    _description = "Result Template"

    exam_session_id = fields.Many2one(
        'op.exam.session', 'Exam Session',
        required=True, track_visibility='onchange')
    evaluation_type = fields.Selection(
        related='exam_session_id.evaluation_type',
        store=True, track_visibility='onchange')
    name = fields.Char("Name", size=254,
                       required=True, track_visibility='onchange')
    result_date = fields.Date(
        'Result Date', required=True,
        default=fields.Date.today(), track_visibility='onchange')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('result_generated', 'Result Generated')
    ], string='State', default='draft', track_visibility='onchange')

    _sql_constraints = [
        ('unique_exam_session',
         'unique(exam_session_id)',
         'Results Template for this session/ Study Period is already created. No double entry'),
    ]

    @api.multi
    @api.constrains('exam_session_id')
    def _check_exam_session(self):
        for record in self:
            for exam in record.exam_session_id.exam_ids:
                if exam.state != 'done':
                    raise ValidationError(
                        _('All course units  exams must fully done.'))

    @api.multi
    def generate_result(self):
        results_array = []
        sub_dict_match = []
        for record in self:
            exam_attendees = self.env['op.exam.attendees'].search([('exam_session', '=', record.exam_session_id.id)])
            for student in exam_attendees:
                sub_result_dict = {'student': student.student_id.id, 'registration_number': student.registration_number,
                                   'course': student.course_id.id,
                                   'semester': student.semester, 'year_of_study': student.year_of_study,
                                   'result_line': [student.id]}
                sub_result_dict2 = {'student': student.student_id.id,
                                    'registration_number': student.registration_number, 'course': student.course_id.id,
                                    'semester': student.semester, 'year_of_study': student.year_of_study}
                results_array.append(sub_result_dict)
                if sub_result_dict2 not in sub_dict_match:
                    sub_dict_match.append(sub_result_dict2)
                else:
                    continue
            expected_results = [[i for i in results_array if each_results.items() <= i.items()]
                                for each_results in sub_dict_match]
            # Using Combine Dictionaries for each student
            final_array = []
            for d in expected_results:
                temp = {}
                for item in d:
                    if temp:
                        temp['result_line'].append(item['result_line'][0])
                    else:
                        temp.update(item)
                final_array.append(temp)

            for each_student in final_array:
                self.env['op.exam.template'].create({
                    'student_id': each_student['student'],
                    'registration_number': each_student['registration_number'],
                    'course_id': each_student['course'],
                    'semester': each_student['semester'],
                    'year_of_study': each_student['year_of_study'],
                    'result_line': [(6, 0, each_student['result_line'])]
                })
            marksheet_reg_id = self.env['op.marksheet.register'].create({
                'name': 'Mark Sheet for %s' % record.exam_session_id.name,
                'exam_session_id': record.exam_session_id.id,
                'generated_date': fields.Date.today(),
                'generated_by': self.env.uid,
                'status': 'draft',
                'result_template_id': record.id
            })
            student_dict = {}
            for exam in record.exam_session_id.exam_ids:
                for attendee in exam.attendees_line:
                    status = 'pass'
                    mark_grade = 'A+'
                    grade_point = 6
                    if attendee.final_marks < exam.min_marks:
                        status = 'fail'
                    if record.evaluation_type == 'grade':
                        grades = attendee.grade_ids
                        for grade in grades:
                            if grade.min_per <= attendee.final_marks <= grade.max_per:
                                mark_grade = grade.result
                                grade_point = grade.grade_point
                    result_line_id = self.env['op.result.line'].create({
                        'student_id': attendee.student_id.id,
                        'registration_number': attendee.registration_number,
                        'exam_id': exam.id,
                        'progressive_marks': attendee.progressive_marks and attendee.progressive_marks or 0,
                        'exam_marks': attendee.exam_marks and attendee.exam_marks or 0,
                        'final_marks': attendee.final_marks and attendee.final_marks or 0,
                        'grade': mark_grade,
                        'grade_point': grade_point,
                    })
                    if attendee.student_id.id not in student_dict:
                        student_dict[attendee.student_id.id] = []
                    student_dict[attendee.student_id.id].append(result_line_id)
                    self.env['op.student.registration.results'].create({
                        'name': attendee.student_id.name,
                        'student_id': attendee.student_id.id,
                        'exam_name': exam.subject_id.name,
                        'exam_id': exam.id,
                        'progressive_marks': attendee.progressive_marks and attendee.progressive_marks or 0,
                        'exam_marks': attendee.exam_marks and attendee.exam_marks or 0,
                        'final_marks': attendee.final_marks and attendee.final_marks or 0,
                        'grade': mark_grade,
                        'grade_point': grade_point,
                        'status': status,
                    })
            for student in student_dict:
                marksheet_line_id = self.env['op.marksheet.line'].create({
                    'student_id': student,
                    'marksheet_reg_id': marksheet_reg_id.id,
                })
                for result_line in student_dict[student]:
                    result_line.marksheet_line_id = marksheet_line_id
            record.state = 'result_generated'
