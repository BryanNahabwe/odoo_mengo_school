# Copyright 2009-2018 Noviat.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import models


class StudentsExportXlsx(models.AbstractModel):
    _name = 'report.openeducat_exam.students_export_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def _get_ws_params(self, wb, data, results):

        student_template = {
            'id': {
                'header': {
                    'value': 'Student System ID',
                },
                'data': {
                    'value': self._render("student.record_external_id"),
                },
                'width': 10,
            },
            'student_id': {
                'header': {
                    'value': 'Student Name',
                },
                'data': {
                    'value': self._render("student_name"),
                },
                'width': 20,
            },
            'registration_number': {
                'header': {
                    'value': 'Registration Number',
                },
                'data': {
                    'value': self._render("student.registration_number"),
                },
                'width': 20,
            },
            'exam_id': {
                'header': {
                    'value': 'Exam / Database ID',
                },
                'data': {
                    'value': self._render("student.exam_id.id"),
                },
                'width': 20,
            },
            'exam_name': {
                'header': {
                    'value': 'Exam Name',
                },
                'data': {
                    'value': self._render("student.exam_name"),
                },
                'width': 20,
            },
            'progressive_marks': {
                'header': {
                    'value': 'Progressive Marks',
                },
                'data': {
                    'value': self._render("progressive_marks"),
                },
                'width': 20,
            },
            'exam_marks': {
                'header': {
                    'value': 'Summative Marks',
                },
                'data': {
                    'value': self._render("exam_marks"),
                },
                'width': 14,
            },
        }

        wanted_list = ['id', 'student_id', 'registration_number', 'exam_id', 'exam_name', 'progressive_marks',
                       'exam_marks']
        ws_params = {
            'ws_name': 'Students',
            'generate_ws_method': '_students_report',
            'title': 'Students',
            'wanted_list': wanted_list,
            'col_specs': student_template,
        }
        return [ws_params]

    def _students_report(self, workbook, ws, ws_params, data, students):
        ws.set_portrait()
        ws.fit_to_pages(1, 0)
        ws.set_header(self.xls_headers['standard'])
        ws.set_footer(self.xls_footers['standard'])
        self._set_column_width(ws, ws_params)

        row_pos = 0
        row_pos = self._write_line(
            ws, row_pos, ws_params, col_specs_section='header',
            default_format=self.format_theader_yellow_left)
        ws.freeze_panes(row_pos, 0)

        wl = ws_params['wanted_list']
        ws.set_column('A:A', None, None, {'hidden': True})
        ws.set_column('D:D', None, None, {'hidden': True})
        for student in students:
            attendees = student.attendees_line
            for attendee in attendees:
                if attendee.progressive_marks == 0:
                    progressive_marks = ''
                else:
                    progressive_marks = attendee.progressive_marks

                if attendee.exam_marks == 0:
                    exam_marks = ''
                else:
                    exam_marks = attendee.exam_marks

                student_name = attendee.student_id.name
                row_pos = self._write_line(
                    ws, row_pos, ws_params, col_specs_section='data',
                    render_space={'student': attendee, 'student_name': student_name,
                                  'progressive_marks': progressive_marks,
                                  'exam_marks': exam_marks},
                    default_format=self.format_tcell_left)
