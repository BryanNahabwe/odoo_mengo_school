import time
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models


class StudentTranscript(models.Model):
    _name = "report.openeducat_exam.report_transcript"
    _description = "Student Transcript Report"

    def get_student_results(self, student_record):
        new_list = []
        student_result_info = self.env['op.exam.template'].search([('student_id', '=', student_record.id)])
        for result in student_result_info:
            gpa = '{:.2f}'.format(result.gpa)
            cgpa = '{:.2f}'.format(result.cgpa)
            res = {
                'year_of_study': result.year_of_study,
                'semester': result.semester,
                'gpa': gpa,
                'cgpa': cgpa,
                'result_line': result.result_line
            }
            new_list.append(res)
        sort_list = sorted(new_list, key=lambda i: (i['year_of_study'], i['semester']))
        final_list = [sort_list[i:i+2] for i in range(0, len(sort_list), 2)]
        return final_list

    def get_total_credit_units(self, student_record):
        student_result_info = self.env['op.exam.template'].search([('student_id', '=', student_record.id)])
        total_cu = 0
        for result in student_result_info:
            total_cu += result.total_credit_units
        total_credit_units = '{:.1f}'.format(total_cu)
        return total_credit_units

    def get_result_lines(self, result_record):
        lines = []
        for line in result_record:
            lines.extend(line)
        return lines

    @api.model
    def _get_report_values(self, docids, data=None):
        student_info = self.env['op.student'].browse([data['student']])
        completion_date = data['completion_date']
        docargs = {
            'doc_ids': self.ids,
            'doc_model': 'op.exam.template',
            'docs': student_info,
            'time': time,
            'completion_date': completion_date,
            'get_student_results': self.get_student_results,
            'get_result_lines': self.get_result_lines,
            'get_total_credit_units': self.get_total_credit_units(student_info)
        }
        return docargs
