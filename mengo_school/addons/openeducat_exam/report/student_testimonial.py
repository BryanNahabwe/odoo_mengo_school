import time

from odoo import api, fields, models


class StudentTestimonial(models.Model):
    _name = "report.openeducat_exam.report_testimonial"
    _description = "Student Testimonial Report"

    def get_student_results(self, student_record):
        new_list = []
        student_result_info = self.env['op.exam.template'].search([('student_id', '=', student_record.id)])
        for result in student_result_info:
            gpa = '{:.2f}'.format(result.gpa)
            cgpa = '{:.2f}'.format(result.cgpa)
            res = {
                'year_of_study': result.year_of_study,
                'semester': result.semester,
                'gpa': gpa,
                'cgpa': cgpa,
                'result_line': result.result_line
            }
            new_list.append(res)
        final_list = sorted(new_list, key=lambda i: (i['year_of_study'], i['semester']))
        return final_list

    def get_result_lines(self, result_record):
        lines = []
        for line in result_record:
            lines.extend(line)
        return lines

    @api.model
    def _get_report_values(self, docids, data=None):
        student_info = self.env['op.student'].browse([data['student']])
        docargs = {
            'doc_ids': self.ids,
            'doc_model': 'op.exam.template',
            'docs': student_info,
            'time': time,
            'get_student_results': self.get_student_results,
            'get_result_lines': self.get_result_lines
        }
        return docargs
