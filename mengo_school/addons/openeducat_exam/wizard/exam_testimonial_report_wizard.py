from odoo import api, fields, models


class ExamTestimonialReportWizard(models.Model):
    _name = 'exam.testimonial.report.wizard'
    _description = 'Exam Testimonial Report Wizard'

    student_id = fields.Many2one('op.student', string='Student', required=True, ondelete='cascade',
                                 domain=lambda self: self._get_default_students())

    def _get_default_students(self):
        return_student_in_exam_template = self.env['op.exam.template'].search([])
        student_ids = []
        for student in return_student_in_exam_template:
            if student.student_id.id not in student_ids:
                student_ids.append(student.student_id.id)
        return [('id', 'in', student_ids)]

    @api.multi
    def print_report(self):
        data = {'student': self.student_id.id}
        return self.env.ref(
            'openeducat_exam.action_student_testimonial_report') \
            .report_action(self, data=data)
