from odoo import api, fields, models


class ExamTranscriptReportWizard(models.Model):
    _name = 'exam.transcript.report.wizard'
    _description = 'Exam Transcript Report Wizard'

    student_id = fields.Many2one('op.student', string='Student', required=True, ondelete='cascade',
                                 domain=lambda self: self._get_default_students())
    completion_date = fields.Date('Completion Date', required=True, default=fields.Date.today())

    def _get_default_students(self):
        return_student_in_exam_template = self.env['op.exam.template'].search([])
        student_ids = []
        for student in return_student_in_exam_template:
            if student.student_id.id not in student_ids:
                student_ids.append(student.student_id.id)
        return [('id', 'in', student_ids)]

    @api.multi
    def print_report(self):
        data = {'student': self.student_id.id, 'completion_date': self.completion_date}
        return self.env.ref(
            'openeducat_exam.action_student_transcript_report') \
            .report_action(self, data=data)
