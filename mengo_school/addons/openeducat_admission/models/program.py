from odoo import models, fields, api


class OpProgramStudent(models.Model):
    _inherit = 'op.program'

    admission_ids = fields.One2many("op.admission", 'program_id', string='Admitted Students')