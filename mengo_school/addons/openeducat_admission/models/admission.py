# -*- coding: utf-8 -*-
##############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError


class OpAdmission(models.Model):
    _name = "op.admission"
    _inherit = "mail.thread"
    _rec_name = "application_number"
    _order = "application_number desc"
    _description = "Admission"

    name = fields.Char(
        'First Name', size=128, required=True)
    middle_name = fields.Char(
        'Middle Name', size=128,
    )
    last_name = fields.Char(
        'Last Name', size=128, required=True,
    )
    title = fields.Many2one(
        'res.partner.title', 'Title', )

    image = fields.Binary('image', )
    state = fields.Selection(
        [('draft', 'Draft'), ('submit', 'Submitted'),
         ('confirm', 'Admitted'), ('reject', 'Rejected'), ('pending', 'Pending'),
         ('cancel', 'Cancelled'), ('done', 'Enrolled'), ('completed', 'Completed')],
        'State', default='draft', track_visibility='onchange')

    # Application Info
    application_number = fields.Char(
        'Application Number', size=16, copy=False,
        readonly=True, store=True,
        states={'submit': [('required', True)]},
        default=lambda self:
        self.env['ir.sequence'].next_by_code('op.admission'))
    admission_date = fields.Date(
        'Admission Date', copy=False, states={'submit': [('required', True)]},
        default=fields.Date.today
    )
    product_id = fields.Many2one('product.product', 'Course Fees Structure', required=True)
    currency_id = fields.Many2one('res.currency', string='Currency',
                                  states={'submit': [('required', True)]},
                                  default=lambda self: self.env.user.company_id.currency_id, track_visibility='always')
    course_id = fields.Many2one(
        'op.course', 'Course',
        states={'submit': [('required', True)]}
    )
    fees = fields.Float('Fees')
    due_date = fields.Date('Due Date')
    student_id = fields.Many2one(
        'op.student', 'Student', )
    nbr = fields.Integer('No of Admission', readonly=True)
    register_id = fields.Many2one(
        'op.admission.register', 'Admission Register',
        states={'submit': [('required', True)]}
    )
    partner_id = fields.Many2one('res.partner', 'Partner')
    fees_term_id = fields.Many2one('op.fees.terms', 'Fees Term',
                                   states={'submit': [('required', True)]})
    program_id = fields.Many2one('op.program', string='Program')

    # Education Background
    prev_institute_id = fields.Char('Previous Institute')
    prev_course_id = fields.Char('Previous Course')
    prev_result = fields.Char('Previous Result', size=256)

    # Application Info
    birth_date = fields.Date(
        'Birth Date', required=True, )
    country_id = fields.Many2one(
        'res.country', 'Nationality', required=True)
    post_code = fields.Char('Post Code', size=16)
    address = fields.Char(
        'Address', size=256, required=True)
    phone = fields.Char(
        'Telephone Number', size=16, required=True)
    email = fields.Char(
        'Email', size=256, required=True,
    )
    application_date = fields.Date(
        'Application Date', copy=False, required=True,
        default=fields.Date.today)

    student_type = fields.Selection([('local', 'Local'),
                                     ('international', 'International')],
                                    'Type of Student',
                                    compute='_compute_student_type', store=True, readonly=True)

    education_qualifications = fields.Selection([('alevel', "A'level"),
                                                 ('certificate', 'Certificate'),
                                                 ('diploma', 'Diploma'),
                                                 ('degree', 'Degree'),
                                                 ('masters', 'Masters')],
                                                'Highest Educational Qualifications (E.g A\' level, Degree)')
    other_qualifications = fields.Char('Other Educational Qualifications')
    medical_conditions = fields.Selection([('yes', 'Yes'),
                                           ('no', 'No')],
                                          'Do you have any medical conditions the Institute should know about?')
    mention_medical_conditions = fields.Text('Please mention the medical condition')
    relevant_courses = fields.Char('Please list any relevant courses that you have attended e.g Workshops')
    course_apply_id = fields.Many2one(
        'op.course', 'Course you are applying for:',
    )
    start_date = fields.Date('Please indicate preferred intake:')
    ambitions = fields.Text('What are your ambitions in Medical Health?')
    referees = fields.Char('If you have a Referee(s), Mentor(s), Please give his/her name(s)')

    # Registration Info
    gender = fields.Selection(
        [('m', 'Male'), ('f', 'Female'), ('o', 'Other')],
        string='Gender',
        states={'submit': [('required', True)]})

    age_groups = fields.Selection([
        ('18 or less', '18 or less'),
        ('19 - 24', '19 - 24'),
        ('25 - 29', '25 - 29'),
        ('30 - 34', '30 - 34'),
        ('35 - 39', '35 - 39'),
        ('40 - 44', '40 - 44'),
        ('45 - 49', '45 - 49'),
        ('50 and above', '50 and above'),
    ], string='Age group: (Please tick one below')
    marital_status = fields.Selection([('single', 'Single'), ('married', 'Married')], string='Marital Status')
    disability = fields.Selection([('yes', 'Yes'), ('no', 'No')],
                                  'Do you have any Disability? If yes, state/describe it and/or attach proof')
    disability_if_any = fields.Char('Add disability Info')
    profession = fields.Char('Profession')
    physical_address = fields.Char('Physical Address')
    region = fields.Char('Region of Origin')
    district = fields.Char('Home District')
    next_of_kin = fields.Char('Next of Kin', size=128)
    kin_tel_number = fields.Char('Kin Tel. Number', size=16)
    sponsors_name = fields.Char('Sponsor\'s Name', size=128)
    sponsors_contact = fields.Char('Sponsor\'s Contact', size=16)
    sponsors_email = fields.Char('Sponsor\'s Email')
    registration_number = fields.Char('Registration Number', states={'submit': [('required', True)]})
    year_of_entry = fields.Char('Year of Entry', size=4, states={'submit': [('required', True)]})
    year_of_study = fields.Selection([('Year 1', 'Year 1'), ('Year 2', 'Year 2'), ('Year 3', 'Year 3'),
                                      ('Year 4', 'Year 4'), ('Year 5', 'Year 5')], 'Year of Study',
                                     states={'submit': [('required', True)]},
                                     default='Year 1')
    semester = fields.Selection([('Semester 1', 'Semester 1'),
                                 ('Semester 2', 'Semester 2'),
                                 ('Trimester 1', 'Trimester 1'),
                                 ('Trimester 2', 'Trimester 2'),
                                 ('Trimester 3', 'Trimester 3'),
                                 ('Module 1', 'Module 1'),
                                 ('Module 2', 'Module 2'),
                                 ('Module 3', 'Module 3'),
                                 ('Module 4', 'Module 4'),
                                 ('Module 5', 'Module 5'),
                                 ('Module 6', 'Module 6'),
                                 ('Module 7', 'Module 7')], default='Semester 1',
                                string='Study Period', states={'submit': [('required', True)]})
    type_of_entry = fields.Selection([('a_level', "A'level"),
                                      ('certificate', 'Certificate'),
                                      ('diploma', 'Diploma'),
                                      ('degree', 'Degree'),
                                      ('masters', 'Masters')], string='Type of Entry',
                                     states={'submit': [('required', True)]})
    in_take = fields.Selection([('january', 'January'),
                                ('march', 'March'),
                                ('april', 'April'),
                                ('july', 'July'),
                                ('august', 'August'),
                                ('september', 'September')], string='In-Take',
                               states={'submit': [('required', True)]}, )
    program_duration = fields.Char('Duration of the Programme')
    year_of_completion = fields.Char('Expected year of Completion', size=4)
    platform_ids = fields.Many2many('op.platform', string='How did you get to know about ECUREI?')
    if_alumni = fields.Char('Please mention his/her name')
    date_registered = fields.Date('Date Registered', default=fields.Date.today, required=True)

    _sql_constraints = [
        ('unique_application_number',
         'unique(application_number)',
         'Application Number must be unique per Application!'),
        ('unique_registration_number',
         'unique(registration_number)',
         'Registration Number must be unique per Application!')
    ]

    @api.multi
    @api.depends('name', 'application_number')
    def name_get(self):
        res = []
        for record in self:
            name = record.name
            last_name = record.last_name
            if record.application_number:
                name = record.application_number + ' ' + name + ' ' + last_name
            res.append((record.id, name))
        return res

    @api.onchange('register_id')
    def onchange_register(self):
        self.course_id = self.register_id.course_id

    @api.multi
    @api.depends('country_id')
    def _compute_student_type(self):
        for record in self:
            if record.country_id.id == 230:
                record.student_type = 'local'

            else:
                record.student_type = 'international'

    @api.onchange('student_type')
    def onchange_student_type(self):
        if self.student_type == 'local':
            self.currency_id = 43
        else:
            self.currency_id = 2

    @api.onchange('country_id')
    def onchange_country_product(self):
        res = {}
        if self.country_id.id == 230:
            student_type = 'local'
            product_templates = self.env['product.template'].search([('student_type', '=', student_type)])
            product_template_ids = []
            for prod in product_templates:
                product_template_ids.append(prod.id)
            products = self.env['product.product'].search([('product_tmpl_id', 'in', product_template_ids)])
            product_ids = []
            for data in products:
                product_ids.append(data.id)
            res['domain'] = {'product_id': [('id', 'in', product_ids)]}

        else:
            student_type = 'international'
            product_templates = self.env['product.template'].search([('student_type', '=', student_type)])
            product_template_ids = []
            for prod in product_templates:
                product_template_ids.append(prod.id)
            products = self.env['product.product'].search([('product_tmpl_id', 'in', product_template_ids)])
            product_ids = []
            for data in products:
                product_ids.append(data.id)
            res['domain'] = {'product_id': [('id', 'in', product_ids)]}
        return res

    @api.onchange('product_id')
    def onchange_product_id(self):
        self.fees = self.product_id.lst_price

    @api.onchange('course_id')
    def onchange_course(self):
        term_id = False
        prog_id = False
        program_duration = False
        if self.course_id and self.course_id.fees_term_id:
            term_id = self.course_id.fees_term_id.id
        if self.course_id and self.course_id.program_id:
            prog_id = self.course_id.program_id.id
            program_duration = self.course_id.program_id.program_duration
        self.fees_term_id = term_id
        self.program_id = prog_id
        self.program_duration = program_duration

    @api.multi
    @api.constrains('register_id', 'admission_date')
    def _check_admission_register(self):
        for rec in self:
            if self.state == 'submit':
                start_date = fields.Date.from_string(rec.register_id.start_date)
                end_date = fields.Date.from_string(rec.register_id.end_date)
                admission_date = fields.Date.from_string(rec.admission_date)
                if admission_date < start_date or admission_date > end_date:
                    raise ValidationError(_(
                        "Admission Date should be between Start Date & \
                        End Date of Admission Register."))

    @api.multi
    @api.constrains('birth_date')
    def _check_birthdate(self):
        for record in self:
            if record.birth_date > fields.Date.today():
                raise ValidationError(_(
                    "Birth Date can't be greater than current date!"))

    @api.multi
    def submit_form(self):
        self.state = 'submit'

    @api.multi
    def confirm_in_progress(self):
        for record in self:
            record.state = 'confirm'

    @api.multi
    def get_student_vals(self):
        for student in self:
            details = {
                'phone': student.phone,
                'mobile': student.phone,
                'email': student.email,
                'street': student.address,
                'country_id':
                    student.country_id and student.country_id.id or False,
                'image': student.image,
            }
            student.partner_id.write(details)
            details.update({
                'title': student.title and student.title.id or False,
                'name': student.name + ' ' + student.last_name,
                'middle_name': student.middle_name,
                'birth_date': student.birth_date,
                'gender': student.gender,
                'address': student.address,
                'age_groups': student.age_groups,
                'marital_status': student.marital_status,
                'district': student.district,
                'registration_number': student.registration_number,
                'state': 'enrolled',
                'year_of_entry': student.year_of_entry,
                'semester': student.semester,
                'type_of_entry': student.type_of_entry,
                'in_take': student.in_take,
                'year_of_study': student.year_of_study,
                'program_duration': student.program_duration or False,
                'year_of_completion': student.year_of_completion,
                'course_id': student.course_id and student.course_id.id or False,
                'program_id': student.program_id and student.program_id.id or False,
                'register_id': student.register_id and student.register_id.id or False,
                'application_id': student and student.id or False,
                'date_registered': student.date_registered,
                'fees_term_id': student.fees_term_id and student.fees_term_id.id or False,
                'fees': student.fees or False,
                'student_type': student.student_type or False,
                'product_id': student.product_id and student.product_id.id or False,
                'currency_id': student.currency_id and student.currency_id.id or False,
                'image': student.image or False,
                'course_detail_ids': [[0, False, {
                    'date': fields.Date.today(),
                    'course_id':
                        student.course_id and student.course_id.id or False,
                    'program_id':
                        student.program_id and student.program_id.id or False,
                }]],
            })
            return details

    @api.multi
    def write(self, vals):
        student = self.env['op.student'].search([('id', '=', self.student_id.id)])
        student.write(vals)
        return super(OpAdmission, self).write(vals)

    @api.multi
    def enroll_student(self):
        for record in self:
            if record.register_id.max_count:
                total_admission = self.env['op.admission'].search_count(
                    [('register_id', '=', record.register_id.id),
                     ('state', '=', 'done')])
                if not total_admission < record.register_id.max_count:
                    msg = 'Max Admission In Admission Register :- (%s)' % (
                        record.register_id.max_count)
                    raise ValidationError(_(msg))
            if not record.student_id:
                vals = record.get_student_vals()
                record.partner_id = record.partner_id.id
                student_id = self.env['op.student'].create(vals).id
            else:
                student_id = record.student_id.id
                record.student_id.write({
                    'course_detail_ids': [[0, False, {
                        'course_id':
                            record.course_id and record.course_id.id or False,
                    }]],
                })
            if record.fees_term_id:
                val = []
                product_id = record.product_id.id
                semester = record.semester
                currency_id = record.currency_id.id
                registration_number = record.registration_number
                course_id = record.course_id.id
                year_of_study = record.year_of_study
                for line in record.fees_term_id.line_ids:
                    no_days = line.due_days
                    per_amount = line.value
                    amount = (per_amount * record.fees) / 100
                    date = (datetime.today() + relativedelta(
                        days=no_days)).date()
                    dict_val = {
                        'fees_line_id': line.id,
                        'amount': amount,
                        'date': date,
                        'product_id': product_id,
                        'currency_id': currency_id,
                        'state': 'draft',
                        'year_of_study': year_of_study,
                        'semester': semester,
                        'course_id': course_id,
                        'registration_number': registration_number
                    }
                    val.append([0, False, dict_val])
                self.env['op.student'].browse(student_id).write({
                    'fees_detail_ids': val
                })
            record.write({
                'nbr': 1,
                'state': 'done',
                'admission_date': fields.Date.today(),
                'student_id': student_id,
            })

    @api.multi
    def students_imported(self):
        submitted_students = self.env['op.student'].search([])
        application_ids = []
        for student in submitted_students:
            application_ids.append(student.application_id.id)
        imported_students = self.env['op.admission'].search([('state', '=', 'done'), ('id', 'not in', application_ids)])
        for record in imported_students:
            if not record.student_id:
                vals = record.get_student_vals()
                record.partner_id = record.partner_id.id
                student_id = self.env['op.student'].create(vals).id
            else:
                student_id = record.student_id.id
                record.student_id.write({
                    'course_detail_ids': [[0, False, {
                        'course_id':
                            record.course_id and record.course_id.id or False,
                    }]],
                })
            record.write({
                'student_id': student_id
            })

    @api.multi
    def confirm_rejected(self):
        self.state = 'reject'

    @api.multi
    def confirm_pending(self):
        self.state = 'pending'

    @api.multi
    def confirm_to_draft(self):
        self.state = 'draft'

    @api.multi
    def confirm_cancel(self):
        self.state = 'cancel'

    @api.multi
    def payment_process(self):
        self.state = 'fees_paid'

    @api.multi
    def open_student(self):
        form_view = self.env.ref('openeducat_core.view_op_student_form')
        tree_view = self.env.ref('openeducat_core.view_op_student_tree')
        value = {
            'domain': str([('id', '=', self.student_id.id)]),
            'view_type': 'form',
            'view_mode': 'tree, form',
            'res_model': 'op.student',
            'view_id': False,
            'views': [(form_view and form_view.id or False, 'form'),
                      (tree_view and tree_view.id or False, 'tree')],
            'type': 'ir.actions.act_window',
            'res_id': self.student_id.id,
            'target': 'current',
            'nodestroy': True
        }
        self.state = 'done'
        return value

    @api.multi
    def create_invoice(self):
        """ Create invoice for fee payment process of student """

        inv_obj = self.env['account.invoice']
        partner_id = self.env['res.partner'].create({'name': self.name})

        account_id = False
        product = self.product_id
        currency_id = self.currency_id.id
        print(currency_id)
        if product.id:
            account_id = product.property_account_income_id.id
        if not account_id:
            account_id = product.categ_id.property_account_income_categ_id.id
        if not account_id:
            raise UserError(
                _('There is no income account defined for this product: "%s". \
                                            You may have to install a chart of account from Accounting \
                                            app, settings menu.') % (product.name,))

        if self.fees <= 0.00:
            raise UserError(
                _('The value of the deposit amount must be positive.'))
        else:
            amount = self.fees
            name = product.name

        invoice = inv_obj.create({
            'name': self.name + ' ' + self.last_name,
            'origin': self.application_number,
            'type': 'out_invoice',
            'reference': False,
            'account_id': partner_id.property_account_receivable_id.id,
            'partner_id': partner_id.id,
            'currency_id': currency_id,
            'invoice_line_ids': [(0, 0, {
                'name': self.name + ' ' + self.last_name,
                'origin': self.application_number,
                'account_id': account_id,
                'price_unit': amount,
                'quantity': 1.0,
                'discount': 0.0,
                'uom_id': self.product_id.uom_id.id,
                'product_id': product.id,
            })],
        })
        invoice.compute_taxes()

        form_view = self.env.ref('account.invoice_form')
        tree_view = self.env.ref('account.invoice_tree')
        value = {
            'domain': str([('id', '=', invoice.id)]),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'account.invoice',
            'view_id': False,
            'views': [(form_view and form_view.id or False, 'form'),
                      (tree_view and tree_view.id or False, 'tree')],
            'type': 'ir.actions.act_window',
            'res_id': invoice.id,
            'target': 'current',
            'nodestroy': True
        }
        self.partner_id = partner_id
        self.state = 'payment_process'
        return value
