from odoo import api, fields, models
from datetime import datetime
from dateutil.relativedelta import relativedelta


class OpStudentRegistration(models.Model):
    _name = 'op.student.registration'
    _rec_name = 'student_id'
    _description = 'Student Registration For Continuing Students'

    student_id = fields.Many2one('op.student', 'Student Name', required=True)
    registration_number = fields.Char('Registration Number')
    year_of_entry = fields.Char('Year of Entry', size=4)
    course_id = fields.Many2one('op.course', 'Course', store=True)
    program_duration = fields.Char('Duration of the Programme')
    year_of_study = fields.Selection([('Year 1', 'Year 1'), ('Year 2', 'Year 2'), ('Year 3', 'Year 3'),
                                      ('Year 4', 'Year 4'), ('Year 5', 'Year 5')], string='Present Year of Study',
                                     required=True,
                                     default='Year 1')
    semester = fields.Selection([('Semester 1', 'Semester 1'),
                                 ('Semester 2', 'Semester 2'),
                                 ('Trimester 1', 'Trimester 1'),
                                 ('Trimester 2', 'Trimester 2'),
                                 ('Trimester 3', 'Trimester 3'),
                                 ('Module 1', 'Module 1'),
                                 ('Module 2', 'Module 2'),
                                 ('Module 3', 'Module 3'),
                                 ('Module 4', 'Module 4'),
                                 ('Module 5', 'Module 5'),
                                 ('Module 6', 'Module 6'),
                                 ('Module 7', 'Module 7')
                                 ], default='Semester 1',
                                string='Study Period', required=True)
    repeating_year = fields.Selection([('yes', 'Yes'),
                                       ('no', 'No')],
                                      string='Are you Repeating the year?')
    repeating_year_yes = fields.Char('If yes, Why?')
    year_of_completion = fields.Char('Expected year of Completion', size=4)
    retakes = fields.Many2many('op.exam.attendees', string='Retake(s)')
    missing_papers = fields.Many2many('op.exam.attendees', string='Missing Paper(s)')
    subject_ids = fields.Many2many('op.subject', string='Course Unit(s)', store=True)
    state = fields.Selection(
        [('draft', 'Draft'), ('submit', 'Submitted'),
         ('confirm', 'Confirmed'), ('cancel', 'Cancelled'), ('done', 'Done')],
        'State', default='draft', track_visibility='onchange')

    program_id = fields.Many2one('op.program', 'Program')
    fees_term_id = fields.Many2one('op.fees.terms', 'Fees Term')
    register_id = fields.Many2one('op.admission.register', 'Admission Register')
    fees = fields.Float('Fees')
    product_id = fields.Many2one(
        'product.product', 'Course Fees')
    currency_id = fields.Many2one('res.currency', string='Currency',
                                  default=lambda self: self.env.user.company_id.currency_id)
    student_type = fields.Selection([('local', 'Local'),
                                     ('international', 'International')],
                                    'Type of Student')
    date_registered = fields.Date('Date Registered')

    @api.multi
    def submit_form(self):
        self.state = 'submit'

    @api.multi
    def confirm_in_progress(self):
        for record in self:
            record.state = 'confirm'

    @api.multi
    def confirm_cancel(self):
        self.state = 'cancel'

    @api.onchange('student_id')
    def onchange_student(self):
        if self.student_id:
            sd = self.student_id
            self.registration_number = sd.registration_number or False
            self.year_of_entry = sd.year_of_entry or False
            self.program_duration = sd.program_duration or False
            self.year_of_completion = sd.year_of_completion or False
            self.course_id = sd.course_id and sd.course_id.id or False
            self.program_id = sd.program_id and sd.program_id.id or False
            self.fees_term_id = sd.fees_term_id and sd.fees_term_id.id or False
            self.register_id = sd.register_id and sd.register_id.id or False
            self.product_id = sd.product_id and sd.product_id.id or False
            self.currency_id = sd.currency_id and sd.currency_id.id or False
            self.fees = sd.fees or False
            self.student_type = sd.student_type or False
        else:
            self.registration_number = False
            self.year_of_entry = False
            self.program_duration = False
            self.year_of_completion = False
            self.course_id = False
            self.retakes = False
            self.fees = False
            self.fees_term_id = False
            self.register_id = False
            self.program_id = False
            self.product_id = False
            self.currency_id = False
            self.student_type = False

    @api.onchange('student_id')
    def onchange_student_id(self):
        if self.student_id:
            self.retakes = [(6, 0, [retake.id for retake in
                                    self.env['op.exam.attendees'].search([('student_id', '=', self.student_id.id),
                                                                          ('status', '=', 'fail')])])]
            self.missing_papers = [(6, 0, [paper.id for paper in
                                           self.env['op.exam.attendees'].search(
                                               [('student_id', '=', self.student_id.id),
                                                ('status', '=', 'absent')])])]

    @api.onchange('course_id')
    def onchange_course_id(self):
        # you may need to empty the many2many field if the user change the parking
        # if not just remove this line
        self.subject_ids = [(5, 0, 0)]  # remove all record from many2many

        if self.course_id:
            self.subject_ids = False
            subjects = self.course_id.subject_ids.ids
            if subjects:
                domain = [('id', 'in', subjects)]
                print("Subject Domain: ", domain)
                return {'domain': {'subject_ids': domain}}
        return {'domain': {'subject_ids': [('id', 'in', [])]}}

    @api.multi
    def confirm_registration(self):
        for record in self:
            if record.fees_term_id:
                val = []
                student_id = record.student_id.id
                product_id = record.product_id.id
                currency_id = record.currency_id.id
                registration_number = record.registration_number
                semester = record.semester
                year_of_study = record.year_of_study
                course_id = record.course_id.id
                for line in record.fees_term_id.line_ids:
                    no_days = line.due_days
                    per_amount = line.value
                    amount = (per_amount * record.fees) / 100
                    date = (datetime.today() + relativedelta(
                        days=no_days)).date()
                    dict_val = {
                        'fees_line_id': line.id,
                        'amount': amount,
                        'date': date,
                        'product_id': product_id,
                        'currency_id': currency_id,
                        'state': 'draft',
                        'year_of_study': year_of_study,
                        'semester': semester,
                        'course_id': course_id,
                        'registration_number': registration_number
                    }
                    val.append([0, False, dict_val])
                self.env['op.student'].browse(student_id).write({
                    'fees_detail_ids': val
                })
            self.state = 'done'


class OpStudentRegistrationResults(models.Model):
    _name = 'op.student.registration.results'
    _description = 'Student Results For Continuing Students'

    student_id = fields.Many2one('op.student', 'Student', ondelete="cascade")
    name = fields.Char('Student Name')
    exam_id = fields.Integer('Exam ID')
    exam_name = fields.Char('Exam Name')
    registration_number = fields.Char('Registration Number')
    progressive_marks = fields.Integer('Progressive Marks')
    exam_marks = fields.Integer('Exam Marks')
    final_marks = fields.Integer('Final Marks')
    grade_point = fields.Float('Grade Point')
    grade = fields.Char('Student Grade')
    status = fields.Char('Status')
