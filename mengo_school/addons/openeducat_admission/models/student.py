from odoo import models, fields, api


class OpStudentEducation(models.Model):
    _inherit = "op.student"

    admission_ids = fields.One2many('op.admission', 'student_id', 'Admission Details')