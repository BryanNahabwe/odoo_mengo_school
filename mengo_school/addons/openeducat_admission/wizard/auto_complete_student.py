from odoo import api, fields, models


class AutoCompleteStudent(models.Model):
    _name = 'auto.complete.student'
    _description = 'Auto Complete Student'

    admission_id = fields.Many2one('op.admission', 'Student Admission')

    @api.model
    def default_get(self, fields):
        res = super(AutoCompleteStudent, self).default_get(fields)
        active_id = self.env.context.get('active_id', False)
        admission = self.env['op.admission'].browse(active_id)
        print(admission)
        res.update({
            'admission_id': admission.id
        })
        return res

    def mark_student_completed(self):
        for admission in self:
            print(admission)
            admission.admission_id.state = 'completed'
            students = self.env['op.student'].search([('application_id', '=', admission.admission_id.id)])
            for student in students:
                print(student)
                student.state = 'completed'
