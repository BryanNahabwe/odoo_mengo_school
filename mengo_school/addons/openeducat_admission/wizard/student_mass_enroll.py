from odoo import api, fields, models


class StudentMassEnroll(models.Model):
    _name = 'student.mass.enroll.wizard'
    _description = 'Student Mass Enrolling'

    @api.multi
    def enroll_all_students(self):
        self.ensure_one()
        active_ids = self.env.context.get('active_ids')
        students = self.env['op.admission'].browse(active_ids)
        students = students.filtered(lambda o: o.state in ['confirm'])
        students.enroll_student()

