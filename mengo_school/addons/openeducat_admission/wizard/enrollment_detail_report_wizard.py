# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
import time

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class EnrollmentDetailReportWizard(models.TransientModel):
    """ Admission Analysis Wizard """
    _name = "enrollment.detail.report.wizard"
    _description = "Wizard For Enrollment Details Report"

    enrollment_filter = fields.Selection(
        [('course', 'Course'), ('courses', 'All Courses')],
        'Course Filter', required=True)
    course_id = fields.Many2one('op.course', string='Course', ondelete='cascade')
    courses_ids = fields.Many2many('op.course', string='All Courses')
    start_date = fields.Date(
        'Start Date', default=time.strftime('%Y-%m-01'), required=True)
    end_date = fields.Date('End Date', default=time.strftime('%Y-%m-01'), required=True)

    @api.onchange('enrollment_filter')
    def onchange_enrollment_filter(self):
        if self.enrollment_filter == 'courses':
            self.courses_ids = [(6, 0, [course.id for course in self.env['op.course'].search([])])]

    @api.multi
    def print_report(self):
        start_date = fields.Date.from_string(self.start_date)
        end_date = fields.Date.from_string(self.end_date)
        if start_date > end_date:
            raise ValidationError(_("End Date cannot be set before Start Date."))

        data = {}
        if self.enrollment_filter == 'course':
            data['enrollment_filter'] = self.enrollment_filter
            data['courses'] = []
            data['courses'].append(self.course_id.id)
            data['start_date'] = start_date
            data['end_date'] = end_date
        else:
            data['enrollment_filter'] = self.enrollment_filter
            data['start_date'] = start_date
            data['end_date'] = end_date
            data['courses'] = []
            for course in self.courses_ids:
                data['courses'].append(course.id)
        report = self.env.ref(
            'openeducat_admission.action_enrollment_analysis_report')
        return report.report_action(self, data=data)
