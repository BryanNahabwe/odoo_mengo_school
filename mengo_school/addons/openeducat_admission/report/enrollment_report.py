from odoo import api, fields, models


class EnrollmentReport(models.Model):
    _name = 'report.openeducat_admission.report_enrollment'
    _description = 'Enrollment Report'

    def get_total_students_enrolled(self, data):
        total_number_students_enrolled = 0
        for course_id in data['courses']:
            student_search = self.env['op.admission'].search_count(
                [('state', '=', 'done'),
                 ('course_id', '=', course_id),
                 ('date_registered', '>=', data['start_date']),
                 ('date_registered', '<=', data['end_date'])])
            total_number_students_enrolled += student_search
        return total_number_students_enrolled

    def get_total_students_admitted(self, data):
        total_number_students_admitted = 0
        for course_id in data['courses']:
            student_search = self.env['op.admission'].search_count(
                [('state', 'in', ['confirm', 'done']),
                 ('course_id', '=', course_id),
                 ('admission_date', '>=', data['start_date']),
                 ('admission_date', '<=', data['end_date'])])
            total_number_students_admitted += student_search
        return total_number_students_admitted

    def get_total_students_applied(self, data):
        total_number_students_applied = 0
        for course_id in data['courses']:
            student_search = self.env['op.admission'].search_count(
                [('state', 'in', ['submit', 'confirm', 'done']),
                 ('course_apply_id', '=', course_id),
                 ('application_date', '>=', data['start_date']),
                 ('application_date', '<=', data['end_date'])])
            total_number_students_applied += student_search
        return total_number_students_applied

    def get_all_course_data(self, data):
        lst = []
        country_id_uganda = self.env['res.country'].search([('name', 'in', ['Uganda'])], limit=1)
        for course_id in data['courses']:
            course_info = self.env['op.course'].search([('id', '=', course_id)])
            male_count = self.env['op.admission'].search_count(
                [('state', '=', 'done'),
                 ('course_id', '=', course_id),
                 ('date_registered', '>=', data['start_date']),
                 ('date_registered', '<=', data['end_date']),
                 ('gender', '=', 'm')])
            female_count = self.env['op.admission'].search_count(
                [('state', '=', 'done'),
                 ('course_id', '=', course_id),
                 ('date_registered', '>=', data['start_date']),
                 ('date_registered', '<=', data['end_date']),
                 ('gender', '=', 'f')])
            local_count = self.env['op.admission'].search_count(
                [('state', '=', 'done'),
                 ('course_id', '=', course_id),
                 ('date_registered', '>=', data['start_date']),
                 ('date_registered', '<=', data['end_date']),
                 ('country_id', '=', country_id_uganda.id)])
            international_count = self.env['op.admission'].search_count(
                [('state', '=', 'done'),
                 ('course_id', '=', course_id),
                 ('date_registered', '>=', data['start_date']),
                 ('date_registered', '<=', data['end_date']),
                 ('country_id', '!=', country_id_uganda.id)])
            res = {
                'course': course_info.name,
                'male_count': male_count,
                'female_count': female_count,
                'local_count': local_count,
                'international_count': international_count,
            }
            lst.append(res)
        return lst

    @api.model
    def _get_report_values(self, docids, data=None):
        docargs = {
            'doc_ids': self.ids,
            'doc_model': 'op.admission',
            'docs': docids,
            'start_date': data['start_date'],
            'end_date': data['end_date'],
            'get_total_students_enrolled': self.get_total_students_enrolled(data),
            'get_total_students_admitted': self.get_total_students_admitted(data),
            'get_total_students_applied': self.get_total_students_applied(data),
            'get_all_course_data': self.get_all_course_data(data),
        }
        return docargs
