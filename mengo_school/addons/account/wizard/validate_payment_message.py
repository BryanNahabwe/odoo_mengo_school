from odoo import api, fields, models, _
from odoo.exceptions import UserError


class ValidatePaymentMessage(models.Model):
    _name = 'validate.payment.message'
    _description = 'Validate Payment Message'

    account_payment_id = fields.Many2one('account.payment', 'Account Payment')

    @api.model
    def default_get(self, fields):
        res = super(ValidatePaymentMessage, self).default_get(fields)
        active_id = self.env.context.get('active_id', False)
        account_payment = self.env['account.payment'].browse(active_id)
        res.update({
            'account_payment_id': account_payment.id
        })
        return res

    def mark_payment_posted(self):
        for record in self:
            if len(record) != 1:
                raise UserError(_("This method should only be called to process a single invoice's payment."))
            return record.account_payment_id.post()
