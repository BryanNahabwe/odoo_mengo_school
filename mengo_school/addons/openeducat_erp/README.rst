This module provide overall education management system.
    Features includes managing
        * Student
        * Lecturer
        * Admission
        * Course
        * Standard
        * Books
        * Library
        * Lectures
        * Exams
        * Marksheet
        * Result
        * Transportation
        * Hostel
