# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from odoo import models, api, fields
from odoo.tools import float_is_zero


class ReportFeesAnalysis(models.AbstractModel):
    _name = "report.openeducat_fees.report_fees_analysis"
    _description = "Fees Report"

    def get_invoice_amount(self, student_id):
        total_amount = 0.0
        total_paid = 0.0
        student_name = student_id.name
        reg_number = student_id.registration_number
        note = ''
        for inv in self.env['account.invoice'].search([
            ('partner_id', '=', student_id.partner_id.id),
            ('state', 'in', ['draft', 'open', 'paid']),
        ]):
            total_amount += inv.amount_total
            if not inv.payment_move_line_ids:
                continue
            if inv.payment_move_line_ids:
                for payment in inv.payment_move_line_ids:
                    if inv.type in ('out_invoice', 'in_refund'):
                        amount = sum(
                            [p.amount for p in payment.matched_debit_ids if p.debit_move_id in inv.move_id.line_ids])
                    elif inv.type in ('in_invoice', 'out_refund'):
                        amount = sum(
                            [p.amount for p in payment.matched_credit_ids if p.credit_move_id in inv.move_id.line_ids])
                    currency = payment.company_id.currency_id
                    amount_to_show = currency._convert(amount, inv.currency_id, payment.company_id,
                                                       inv.date or fields.Date.today())
                    if payment.amount_residual == 0:
                        total_paid += amount_to_show
                    elif payment.amount_residual != 0:
                        total_paid += payment.credit
        if total_amount == 0.0:
            note = 'Invoice not created'
        else:
            note = ''
        print("{} {}".format("Student Name", student_name))
        print("{} {}".format("Student Registration", reg_number))
        print("{} {}".format("Tuition", total_amount))
        print("{} {}".format("Total Amount Paid", total_paid))
        return [student_name, reg_number, total_amount, total_paid, note]

    @api.model
    def _get_report_values(self, docids, data=None):
        student_ids = []
        if data['fees_filter'] == 'student':
            student_ids = self.env['op.student'].browse([data['student']])
        else:
            student_ids = self.env['op.student'].search(
                [('course_detail_ids.course_id', '=', data['course'])])
        docargs = {
            'doc_ids': self.ids,
            'doc_model': 'op.student',
            'docs': student_ids,
            'get_invoice_amount': self.get_invoice_amount,
        }
        return docargs
