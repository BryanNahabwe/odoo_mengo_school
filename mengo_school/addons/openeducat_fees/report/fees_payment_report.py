import time
from odoo import api, fields, models


class FeesPaymentRecord(models.Model):
    _name = "report.openeducat_fees.report_fees_payment"
    _description = "Fees Payment Report"

    def get_all_fees_payments(self, course_record, semester_selected, year_of_study):
        new_list = []
        invoice_info = self.env['account.invoice'].search([('course_id', '=', course_record.id),
                                                           ('semester', '=', semester_selected),
                                                           ('year_of_study', '=', year_of_study),
                                                           ('state', '!=', 'cancel')])
        for inv in invoice_info:
            partner_id = inv.partner_id
            total_amount = inv.amount_total
            balance = inv.residual
            amount_paid = total_amount - balance
            res = {'student_name': partner_id.name,
                   'registration_number': inv.registration_number,
                   'course': course_record.code,
                   'currency_id': inv.currency_id.symbol,
                   'total_amount': "{:0,.0f}".format(float(total_amount)),
                   'total_balance': "{:0,.0f}".format(float(balance)),
                   'amount_paid': "{:0,.0f}".format(float(amount_paid))}
            new_list.append(res)
        final_list = new_list
        return final_list

    @api.model
    def _get_report_values(self, docids, data=None):
        course_info = self.env['op.course'].browse([data['course']])
        course_name = course_info.name
        semester = data['semester']
        year_of_study = data['year_of_study']
        docargs = {
            'doc_ids': self.ids,
            'doc_model': 'op.course',
            'course_name': course_name,
            'semester': semester,
            'year_of_study': year_of_study,
            'docs': course_info,
            'time': time,
            'get_all_fees_payments': self.get_all_fees_payments(course_info, semester, year_of_study)
        }
        return docargs
