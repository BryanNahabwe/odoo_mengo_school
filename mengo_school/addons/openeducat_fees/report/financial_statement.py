import time
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models


class FinancialStatement(models.Model):
    _name = "report.openeducat_fees.report_financial_statement"
    _description = "Financial Statement Report"

    def get_student_fees(self, student_record):
        new_list = []
        invoice_info = self.env['account.invoice'].search([('student_id', '=', student_record.id)])
        for inv in invoice_info:
            invoice_info_record = {
                'year_of_study': inv.year_of_study,
                'semester': inv.semester,
                'currency_id': inv.currency_id.name,
                'payment_info': [{'date': inv.date_invoice, 'invoice_no': inv.number,
                                  'reference': '', 'debit': "{:0,.0f}".format(float(inv.amount_total)),
                                  'credit': 0}]
            }
            print("invoice payment info: ", inv._get_payments_vals())
            total_paid = 0.0
            total_balance = 0.0
            if not inv.payment_move_line_ids:
                total_balance = inv.amount_total - total_paid
                payment_record = {'year_of_study': inv.year_of_study, 'semester': inv.semester,
                                  'currency_id': inv.currency_id.name,
                                  'payment_info': [{'date': inv.date_invoice, 'invoice_no': inv.number,
                                                    'reference': '',
                                                    'debit': "{:0,.0f}".format(float(inv.amount_total)),
                                                    'credit': 0}], 'total_paid': "{:0,.0f}".format(float(0.0)),
                                  'total_balance': "{:0,.0f}".format(float(total_balance))}
                new_list.append(payment_record)
            elif inv.payment_move_line_ids:
                invoice_payment_info = inv._get_payments_vals()
                for data in invoice_payment_info:
                    total_paid += data['amount']
                total_balance = inv.amount_total - total_paid
                for payment in inv.payment_move_line_ids:
                    if inv.type in ('out_invoice', 'in_refund'):
                        amount = sum(
                            [p.amount for p in payment.matched_debit_ids if p.debit_move_id in inv.move_id.line_ids])
                    elif inv.type in ('in_invoice', 'out_refund'):
                        amount = sum(
                            [p.amount for p in payment.matched_credit_ids if p.credit_move_id in inv.move_id.line_ids])
                    currency = payment.company_id.currency_id
                    amount_to_show = currency._convert(amount, inv.currency_id, payment.company_id,
                                                       inv.date or fields.Date.today())
                    if payment.amount_residual == 0:
                        total_credit = amount_to_show
                        sub_res = {'date': payment.payment_id.payment_date,
                                   'invoice_no': payment.payment_id.communication,
                                   'reference': payment.payment_id.name, 'debit': 0,
                                   'credit': "{:0,.0f}".format(float(total_credit)),
                                   'note': ''}
                        invoice_info_record['payment_info'].append(sub_res)
                    elif payment.amount_residual != 0:
                        total_credit = payment.credit
                        sub_res = {'date': payment.payment_id.payment_date,
                                   'invoice_no': payment.payment_id.communication,
                                   'reference': payment.payment_id.name, 'debit': 0,
                                   'credit': "{:0,.0f}".format(float(total_credit)),
                                   'note': ''}
                        invoice_info_record['payment_info'].append(sub_res)
                    invoice_info_record['total_paid'] = "{:0,.0f}".format(float(total_paid))
                    invoice_info_record['total_balance'] = "{:0,.0f}".format(float(total_balance))
                new_list.append(invoice_info_record)
        print("New List:   ",  new_list)
        final_list = sorted(new_list, key=lambda i: (i['year_of_study'], i['semester']))
        return final_list

    @api.model
    def _get_report_values(self, docids, data=None):
        student_info = self.env['op.student'].browse([data['student']])
        docargs = {
            'doc_ids': self.ids,
            'doc_model': 'op.student',
            'docs': student_info,
            'time': time,
            'get_student_fees': self.get_student_fees(student_info)
        }
        return docargs
