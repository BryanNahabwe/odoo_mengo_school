from odoo import api, fields, models


class AutoCreateInvoice(models.Model):
    _name = 'invoice.mass.create.wizard'
    _description = 'Invoice Mass Creation'

    @api.multi
    def create_all_invoices(self):
        self.ensure_one()
        active_ids = self.env.context.get('active_ids')
        students = self.env['op.student.fees.details'].browse(active_ids)
        students_ids = students.filtered(lambda o: o.state in ['draft'])
        for student in students_ids:
            student.get_invoice()
