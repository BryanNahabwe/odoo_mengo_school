from odoo import api, fields, models


class FeesPaymentReportWizard(models.Model):
    _name = 'fees.payment.report.wizard'
    _description = 'Fees Payment Report Wizard'

    course_id = fields.Many2one('op.course', string='Course', required=True, ondelete='cascade')
    year_of_study = fields.Selection([('Year 1', 'Year 1'), ('Year 2', 'Year 2'), ('Year 3', 'Year 3'),
                                      ('Year 4', 'Year 4'), ('Year 5', 'Year 5')], string='Year of Study',
                                     default='Year 1', required=True)
    semester = fields.Selection([('Semester 1', 'Semester 1'),
                                 ('Semester 2', 'Semester 2'),
                                 ('Trimester 1', 'Trimester 1'),
                                 ('Trimester 2', 'Trimester 2'),
                                 ('Trimester 3', 'Trimester 3'),
                                 ('Module 1', 'Module 1'),
                                 ('Module 2', 'Module 2'),
                                 ('Module 3', 'Module 3'),
                                 ('Module 4', 'Module 4'),
                                 ('Module 5', 'Module 5'),
                                 ('Module 6', 'Module 6'),
                                 ('Module 7', 'Module 7')], default='Semester 1',
                                string='Study Period', required=True)

    @api.multi
    def print_report(self):
        data = {'course': self.course_id.id, 'semester': self.semester, 'year_of_study': self.year_of_study}
        return self.env.ref(
            'openeducat_fees.action_report_student_fees_payment_report') \
            .report_action(self, data=data)