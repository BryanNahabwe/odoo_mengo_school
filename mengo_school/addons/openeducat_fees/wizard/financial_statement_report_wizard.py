from odoo import api, fields, models


class FinancialStatementReportWizard(models.Model):
    _name = 'financial.statement.report.wizard'
    _description = 'Financial Statement Report Wizard'

    student_id = fields.Many2one('op.student', string='Student', required=True, ondelete='cascade')

    @api.multi
    def print_report(self):
        data = {'student': self.student_id.id}
        return self.env.ref(
            'openeducat_fees.action_report_student_financial_statement') \
            .report_action(self, data=data)