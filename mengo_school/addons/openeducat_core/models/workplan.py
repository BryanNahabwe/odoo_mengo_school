from odoo import models, fields


class Platform(models.Model):
    _name = "op.workplan"
    _rec_name = 'subject_id'
    _description = "Lecturers  WorkPlan"

    course_id = fields.Many2one(
        'op.course', 'Course', required=True, track_visibility='onchange')
    subject_id = fields.Many2one(
        'op.subject', 'Course Unit', track_visibility='onchange')
    faculty_id = fields.Many2one('op.faculty', 'Lecturer')
    workplan = fields.Binary(string="Upload Work Plan")
    file_name = fields.Char(string="File Name")
