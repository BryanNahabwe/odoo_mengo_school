from odoo import models, fields, api


class Program(models.Model):
    _name = 'op.program'
    _description = 'New Programs'

    name = fields.Char(string="Name", required=True, )
    code = fields.Char(string="Code", required=True, )
    program_duration = fields.Char('Duration of the Programme')
    course_ids = fields.One2many('op.course', 'program_id', string='Courses')

