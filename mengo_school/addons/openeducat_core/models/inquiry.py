from odoo import models, fields, api
from odoo.exceptions import ValidationError


class OpInquiry(models.Model):
    _name = 'op.inquiry'
    _description = 'New Inquiry Section'

    name = fields.Char(string="Name", required=True, )
    gender = fields.Selection(string="Gender", selection=[('m', 'Male'), ('f', 'Female'), ('o', 'Other')],
                              required=True, default='m', )
    contact = fields.Char(string='Contact', required=True)
    inquiry_details = fields.Text(string="Inquiry Details", help="Add the inquiry information here")
    time_in = fields.Datetime('Time In', required=True, default=lambda self: fields.Datetime.now())
    time_out = fields.Datetime('Time Out', required=True, default=lambda self: fields.Datetime.now())
    address = fields.Char(string='Address')
    id_number = fields.Char(string='ID Number (NIN)', size=20)

    @api.multi
    @api.constrains('time_in', 'time_out')
    def _check_time_in(self):
        for record in self:
            if record.time_in >= record.time_out:
                raise ValidationError((
                    "Time Out must be greater than Time in!"))
