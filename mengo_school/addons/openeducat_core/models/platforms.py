from odoo import models, fields


class Platform(models.Model):
    _name = "op.platform"
    _description = "Adversting Platforms"

    name = fields.Char(string="Platform Name")
    code = fields.Char(string="Code")
