# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class OpStudent(models.Model):
    _name = "op.student"
    _description = "Student"
    _rec_name = "display_name"
    _inherits = {"res.partner": "partner_id"}

    middle_name = fields.Char('Middle Name', size=128)
    display_name = fields.Char('Name', compute='_compute_display_name')
    # last_name = fields.Char('Last Name', size=128)
    image = fields.Binary('image', )
    birth_date = fields.Date('Birth Date')
    gender = fields.Selection([
        ('m', 'Male'),
        ('f', 'Female'),
        ('o', 'Other')
    ], 'Gender', required=True, default='m')
    country_id = fields.Many2one('res.country', 'Nationality')
    id_number = fields.Char('ID Card Number', size=64)
    already_partner = fields.Boolean('Already Partner')
    partner_id = fields.Many2one('res.partner', 'Partner',
                                 required=True, ondelete="cascade")
    gr_no = fields.Char("GR Number", size=20)
    course_detail_ids = fields.One2many('op.student.course', 'student_id',
                                        'Course Details',
                                        track_visibility='onchange')
    address = fields.Char(
        'Address', size=256, )
    age_groups = fields.Selection([
        ('18 or less', '18 or less'),
        ('19 - 24', '19 - 24'),
        ('25 - 29', '25 - 29'),
        ('30 - 34', '30 - 34'),
        ('35 - 39', '35 - 39'),
        ('40 - 44', '40 - 44'),
        ('45 - 49', '45 - 49'),
        ('50 and above', '50 and above'),
    ], string='Age group')
    marital_status = fields.Selection([('single', 'Single'), ('married', 'Married')], string='Marital Status')
    district = fields.Char('Home District')
    registration_number = fields.Char('Registration Number', required=True)
    year_of_entry = fields.Char('Year of Entry', size=4)
    year_of_study = fields.Selection([('Year 1', 'Year 1'), ('Year 2', 'Year 2'), ('Year 3', 'Year 3'),
                                      ('Year 4', 'Year 4'), ('Year 5', 'Year 5')], string='Year of Study',
                                     default='Year 1')
    semester = fields.Selection([('Semester 1', 'Semester 1'),
                                 ('Semester 2', 'Semester 2'),
                                 ('Trimester 1', 'Trimester 1'),
                                 ('Trimester 2', 'Trimester 2'),
                                 ('Trimester 3', 'Trimester 3'),
                                 ('Module 1', 'Module 1'),
                                 ('Module 2', 'Module 2'),
                                 ('Module 3', 'Module 3'),
                                 ('Module 4', 'Module 4'),
                                 ('Module 5', 'Module 5'),
                                 ('Module 6', 'Module 6'),
                                 ('Module 7', 'Module 7')
                                 ], default='Semester 1',
                                string='Study Period')
    type_of_entry = fields.Selection([('a_level', 'A\' Level'),
                                      ('certificate', 'Certificate'),
                                      ('diploma', 'Diploma'),
                                      ('degree', 'Degree'),
                                      ('masters', 'Masters')], string='Type of Entry')
    in_take = fields.Selection([('january', 'January'),
                                ('march', 'March'),
                                ('april', 'April'),
                                ('july', 'July'),
                                ('august', 'August'),
                                ('september', 'September')], string='In-Take')
    program_duration = fields.Char('Duration of the Programme')
    year_of_completion = fields.Char('Expected year of Completion', size=4)
    course_id = fields.Many2one('op.course', 'Course')
    program_id = fields.Many2one('op.program', 'Program')
    fees_term_id = fields.Many2one('op.fees.terms', 'Fees Term')
    register_id = fields.Many2one('op.admission.register', 'Admission Register')
    application_id = fields.Many2one('op.admission', 'Application')
    date_registered = fields.Date('Date Registered', default=fields.Date.today)
    fees = fields.Float('Fees')
    product_id = fields.Many2one(
        'product.product', 'Course Fees')
    currency_id = fields.Many2one('res.currency', string='Currency',
                                  default=lambda self: self.env.user.company_id.currency_id)
    student_type = fields.Selection([('local', 'Local'),
                                     ('international', 'International')],
                                    'Type of Student')
    state = fields.Selection(
        [('enrolled', 'Enrolled'), ('completed', 'Completed')],
        'State', default='enrolled', track_visibility='onchange')

    _sql_constraints = [(
        'unique_registration_number',
        'unique(registration_number)',
        'Registration Number must be unique per Student!'
    )]

    @api.multi
    @api.depends('name', 'middle_name')
    def _compute_display_name(self):
        for record in self:
            name = '%s' % (record.name or '')
            middle_name = '%s' % (record.middle_name or '')
            record.display_name = _("%s %s") % (name, middle_name)

    @api.multi
    @api.constrains('birth_date')
    def _check_birthdate(self):
        for record in self:
            if record.birth_date > fields.Date.today():
                raise ValidationError(_(
                    "Birth Date can't be greater than current date!"))

    @api.model
    def get_import_templates(self):
        return [{
            'label': _('Import Template for Students'),
            'template': '/openeducat_core/static/xls/op_student.xls'
        }]


class OpStudentCourse(models.Model):
    _name = "op.student.course"
    _description = "Student Course Details"

    student_id = fields.Many2one('op.student', 'Student', ondelete="cascade")
    course_id = fields.Many2one('op.course', 'Course', required=True)
    program_id = fields.Many2one('op.program', 'Program')
    roll_number = fields.Char('Roll Number')
    subject_ids = fields.Many2many('op.subject', string='Course Units')
