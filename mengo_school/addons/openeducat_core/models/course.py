# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class OpCourse(models.Model):
    _name = "op.course"
    _inherit = "mail.thread"
    _description = "OpenEduCat Course"

    name = fields.Char('Name', required=True)
    code = fields.Char('Code', size=16, required=True)
    evaluation_type = fields.Selection(
        [('grade', 'Grade'),
         ('GPA', 'GPA'),
         ('CWA', 'CWA'),
         ('CCE', 'CCE')],
        'Evaluation Type', default="grade", required=True, track_visibility='onchange')
    subject_ids = fields.Many2many('op.subject', string='Course Unit(s)')
    grade_ids = fields.Many2many('op.grade.configuration', string='Grade Configuration')
    max_unit_load = fields.Float("Maximum Unit Load")
    min_unit_load = fields.Float("Minimum Unit Load")
    program_id = fields.Many2one('op.program', 'Program')

    _sql_constraints = [
        ('unique_course_code',
         'unique(code)', 'Code should be unique per course!')]

    @api.model
    def get_import_templates(self):
        return [{
            'label': _('Import Template for Courses'),
            'template': '/openeducat_core/static/xls/op_course.xls'
        }]

    @api.multi
    @api.constrains('grade_ids')
    def _check_min_max_per(self):
        for record in self:
            count = 0
            for grade in record.grade_ids:
                for sub_grade in record.grade_ids:
                    if grade != sub_grade:
                        if (sub_grade.min_per <= grade.min_per <= sub_grade.max_per) or \
                                (sub_grade.min_per <= grade.max_per <= sub_grade.max_per):
                            count += 1
            if count > 0:
                raise ValidationError(_('Percentage range conflict with other record.'))
